﻿using System;

namespace TernOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = null;
            var f = new Foo(s);
        }
        abstract class Base
        {
            public Base(int i)
            {

            }
        }

        class Foo : Base
        {
            public Foo(string s) : base(s != null ? s.Length : 0)
            {

            }
        }
    }
}
