﻿using System;
using System.Collections.Generic;

namespace InheritPolim
{
    class Program
    {
        static void Main(string[] args)
        {
            var mass = new List<Movements> { new Boat(),new Car(),new Airplane()};
            foreach (var perem in mass)
            {
                perem.Display();
            }
        }
    }

    public abstract class Movements
    {
        public abstract void Display();    
    }

    class Boat : Movements
    {
        public override void Display()
        {
            Console.WriteLine("Плыть");
        }
    }

    class Car : Movements
    {
        public override void Display()
        {
            Console.WriteLine("Ехать");
        }
    }

    class Airplane : Movements
    {
        public override void Display()
        {
            Console.WriteLine("Лететь");
        }
    }
}