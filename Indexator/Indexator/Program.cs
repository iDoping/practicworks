﻿using System;

namespace Indexator
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Nums();
            Console.WriteLine(a[5]);
        }
    }

    class Nums
    {
        int[] nums = { 0, 1, 2, 3, 4, 5, 6 };

        public int this[int index]
        {
            get
            {
                return nums[index];
            }

            set
            {
                nums[index] = value;
            }
        }
    }
}
