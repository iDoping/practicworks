﻿using System;

namespace TypS
{
    class Program
    {
        static void Main(string[] args)
        {
            var id = "test";
            myMiles miles1 = new myMiles {Value = 0};
            myMiles miles2 = new myMiles { Value = 80 };
            myKilometrs kilos1 = new myKilometrs { Value = 40 };
            myKilometrs kilos2 = new myKilometrs { Value = 80 };
            Console.WriteLine($"Путь в милях: {SaveMilesToDB(id, miles1, miles2)}");
            Console.WriteLine($"Путь в километрах: {SaveKilometrsToDB(id, kilos1, kilos2)}");
        }
        static double SaveMilesToDB(string cargoId, myMiles milePath1, myMiles milePath2)
        {
            return (milePath1 + milePath2).Value;
        }

        static double SaveKilometrsToDB(string cargoId, myKilometrs kmPath1, myKilometrs kmPath2)
        {
            return (kmPath1 + kmPath2).Value; ;
        }

        struct myMiles
        {
            private double _value;
            public double Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    if (value <= 0)
                    {
                        throw new ArgumentException("Количество миль должно быть больше нуля и не отрицательным числом");
                    }
                    _value = value;
                }
            }

            public static myMiles operator +(myMiles n1, myMiles n2)
            {
                return new myMiles { Value = n1.Value + n2.Value };
            }

            public static myMiles operator *(myMiles n1, myMiles n2)
            {
                return new myMiles { Value = n1.Value * n2.Value };
            }

            public static myMiles operator /(myMiles n1, myMiles n2)
            {
                return new myMiles { Value = n1.Value / n2.Value };
            }

            public static myMiles operator -(myMiles n1, myMiles n2)
            {
                return new myMiles { Value = n1.Value - n2.Value };
            }
        }

        struct myKilometrs
        {
            private double _value;
            public double Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    if (value <= 0))
                    {
                        throw new ArgumentException("Количество миль должно быть больше нуля и не отрицательным числом");
                    }
                    _value = value;
                }
            }

            public static myKilometrs operator +(myKilometrs n1, myKilometrs n2)
            {
                return new myKilometrs { Value = n1.Value + n2.Value };
            }
            public static myKilometrs operator *(myKilometrs n1, myKilometrs n2)
            {
                return new myKilometrs { Value = n1.Value * n2.Value };
            }

            public static myKilometrs operator /(myKilometrs n1, myKilometrs n2)
            {
                return new myKilometrs { Value = n1.Value / n2.Value };
            }

            public static myKilometrs operator -(myKilometrs n1, myKilometrs n2)
            {
                return new myKilometrs { Value = n1.Value - n2.Value };
            }
        }
    }
}
